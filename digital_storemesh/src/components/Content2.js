import React from 'react'
import Footer from './Footer'
import Header from './Header'
import './Publication.css'

function Content2() {
    return (
        <div>
            <Header/>
            <div className="bg">
                <div className="publication-banner-con">
                    <div className="publication-banner-text">
                        <h1>Software Engineer <br/>
                            เบื้องหลังคนสำคัญในการพัฒนาซอฟต์แวร์
                        </h1>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default Content2
