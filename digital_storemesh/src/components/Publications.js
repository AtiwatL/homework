import React from 'react'
import Footer from './Footer'
import Header from './Header'
import './Publication.css'

function Publications() {
    return (
        <div>
            <Header />
            <div className="bg">
                <div className="publication-banner-con">
                    <div className="publication-banner-text">
                        <h1>Our Reasearch Publications<br />
                            — Deep Learning, Computer Vision, Knowledge Graph Analytics, GPU and Big Data Computer Cluster —
                        </h1>
                    </div>
                </div>
            </div>

            <div className="container">
                <section className="content-con">
                    <div className="content">
                        <p>
                            Warin, Kritsasith, Wasit Limprasert, Siriwan Suebnukarn, Suthin Jinaporntham, Patcharapon Jantana, <br/>
                            and Sothana Vicharueang. 2021. “AI-Based Analysis of Oral Lesions Using Novel Deep Convolutional<br/>
                            Neural Networks for Early Detection of Oral Cancer.”
                        </p>
                        <p>
                            Warin, K., W. Limprasert, S. Suebnukarn, S. Jinaporntham, and P. Jantana. 2021. “Performance of Deep <br/>
                            Convolutional Neural Network for Classification and Detection of Oral Potentially Malignant Disorders <br/>
                            in Photographic Images.” International Journal of Oral and Maxillofacial Surgery, September. <br/>
                            https://doi.org/10.1016/j.ijom.2021.09.001.
                        </p>
                        <p>
                            Warin, Kritsasith, Wasit Limprasert, Siriwan Suebnukarn, Suthin Jinaporntham, and Patcharapon Jantana. <br/>
                            "Automatic classification and detection of oral cancer in photographic images using deep learning algorithms.<br/>
                            " Journal of Oral Pathology & Medicine (2021).
                        </p>
                    </div>
                </section>
            </div>
            <Footer />
        </div>
    )
}

export default Publications
