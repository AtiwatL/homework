import React from 'react'
import './Footer.css'
import logofooter from '../images/logo.png'


function Footer() {
    return (
        <div className="footer">
            <div className="footer-con">
                <div className="footer-con-l">
                    <img src={logofooter} alt="" />
                </div>
                <div className="footer-con-r">
                    <p>
                        บริษัท ดิจิทัลสโตร์เมซ จำกัด (สำนักงานใหญ่) 131 อาคารกลุ่มนวัตกรรม 1 อุทยานวิทยาศาสตร์ประเทศไทย ถนนพหลโยธิน <br/> 
                        ตำบลคลองหนึ่ง อำเภอคลองหลวง ปทุมธานี 12120  Tel: 02-147-0789, 085-842-3747 <br/> 
                        email: contact@storemesh.com 
                    </p>
                </div>
            </div>
        </div>
    )
}

export default Footer
