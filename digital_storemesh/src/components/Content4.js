import React from 'react'
import Footer from './Footer'
import Header from './Header'

function Content4() {
    return (
        <div>
            <Header/>
            <div className="bg">
                <div className="publication-banner-con">
                    <div className="publication-banner-text">
                        <h1>Data Analytics เพื่อการส่งออก</h1>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default Content4
