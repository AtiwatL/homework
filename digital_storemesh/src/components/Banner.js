import React from 'react'
import './Banner.css'

let bannerData = {
    title: "Stomesh Connecting Business",
    desc: "— Thailand based Software Development Team, ERP integration, Data Governance Platform and Artificial Intelligence —"
}

function Banner() {
    return (
        <div className="banner-bg">
            <div className="container">
                <div className="banner-con">
                    <div className="banner-text">
                        <h1>{bannerData.title}</h1>
                        <p>{bannerData.desc}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Banner
