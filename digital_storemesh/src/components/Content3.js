import React from 'react'
import Footer from './Footer'
import Header from './Header'

function Content3() {
    return (
        <div>
            <Header/>
            <div className="bg">
                <div className="publication-banner-con">
                    <div className="publication-banner-text">
                        <h1>Data Science <br/>
                            จำเป็นต่อธุรกิจอย่างไร 
                        </h1>
                    </div>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

export default Content3
