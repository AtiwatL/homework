import React from 'react'
import Footer from './Footer'
import Header from './Header'

function Content1() {
    return (
        <div>
            <Header/>
            <div className="bg">
                <div className="publication-banner-con">
                    <div className="publication-banner-text">
                        <h1>ทำความรู้จัก Front-end Developer <br />
                                อาชีพยอดนิยมในยุคดิจิทัล
                        </h1>
                    </div>
                </div>
            </div>

            <div className="container">
                <section className="content-con">
                    <div className="image-l">
                        <img src="https://media.istockphoto.com/photos/concentrated-male-in-casual-working-or-typing-on-smart-keyboard-for-picture-id1164823514?b=1&k=20&m=1164823514&s=170667a&w=0&h=lXHXtOS1JRTg9Sijs_EQEj6dCeI_zxdIMnQ23yqYiW8=" alt="" />
                    </div>
                    <div className="content2-r">
                        <h3>ความเชี่ยวชาญของเรา</h3>
                        <p>
                            ทีมงานของเรามีประสบการณ์ในการ<br/>
                            ดำเนินโครงการภาครัฐและเอกชน <br/>
                            ด้านการพัฒนาระบบจัดการข้อมูล<br/>
                            ขนาดใหญ่
                        </p>
                    </div>
                </section>
                <section className="content-con">
                    <div className="content-text">
                        <p>

                        </p>
                    </div>
                </section>
            </div>
            <Footer/>
        </div>
    )
}

export default Content1
