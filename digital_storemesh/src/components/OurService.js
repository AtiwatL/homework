import React from 'react'
import Footer from './Footer'
import Header from './Header'

function OurService() {
    return (
        <div>
            <Header/>
            <Footer/>
        </div>
    )
}

export default OurService
