import React from 'react'
import './Content.css'

let bannerData = {
    title: "Data Platform",
    desc: "ระบบบริหารจัดการข้อมูลขนาดใหญ่แบบครบวงจร"
}

function Content() {
    return (
        <div className="container">
            <div className="title-con">
                <div className="title-text">
                    <h1>{bannerData.title}</h1>
                    <p>{bannerData.desc}</p>
                </div>
            </div>
            <section className="content-con">
                <div className="content-l">
                    <img src="https://media.istockphoto.com/photos/trade-platform-forex-trading-stock-exchange-market-analysis-on-laptop-picture-id1306957182?b=1&k=20&m=1306957182&s=170667a&w=0&h=MKCu806tKTf7HRCHRof-hT7FskZ0JMeCgXkKYeuLX94=" alt="" />
                </div>
                <div className="content-r">
                    <p>
                        เราสร้างแพลตฟอร์มสำหรับจัดการข้อมูลตลอด
                        วัฏจักรของข้อมูล (Data Lifecycle) ที่มีประสิทธิภาพ 
                        สามารถช่วยคุณจัดการข้อมูลธุรกิจ ทั้งนำเข้าข้อมูล 
                        สืบค้น ตรวจสอบ วิเคราะห์ ตลอดจนนำเสนอได้
                        อย่างง่ายดายโดยการพัฒนาซอฟต์แวร์
                        ดำเนินการตามมาตราฐาน ISO29110
                        อ่านเพิ่มเติม
                    </p>
                </div>
            </section>
            <div className="title-con">
                <div className="title-text">
                    <h1>Our Srevices</h1>
                </div>
            </div>
            <section className="content-con">
                <div className="content-l">
                    <h3>Software Development</h3>
                    <p>
                        บริการให้คำปรึกษา ออกแบบ และพัฒนาซอฟต์แวร์ ให้ทำงานได้ตรงกับความ ต้องการ หรือ แก้ไขปัญหาของคุณ โดยใช้เทคโนโลยีที่ทันสมัยมีประสิทธิภาพผ่านมาตราฐาน ISO29110 อ่านต่อ
                    </p>
                </div>
            </section>
            <section className="content-con">
                <div className="content-r">
                    <h3>Data Analytics</h3>
                    <p>
                        บริการให้คำปรึกษา รวบรวมข้อมูลและวิเคราะห์ข้อมูลเพื่อนำเสนอ สิ่งที่เป็นประโยชน์แก่คุณสามารถวิเคราะห์ได้ทั้งข้อมูลส่วนบุคคลและข้อมูลในระดับองค์กร อ่านต่อ
                    </p>
                </div>
            </section>
            <section className="content-con">
                <div className="content-l">
                    <h3>IT Training</h3>
                    <p>
                    บริการอบรมทางด้าน IT เพื่อเพิ่มทักษะให้กับบุคลากรของคุณไม่ว่าจะเป็นการพัฒนาเว็บไซต์ การบริหารและจัดการข้อมูล การวิเคราะห์ข้อมูล และการออกแบบ อ่านต่อ
                    </p>
                </div>
            </section>
            <div className="title-con-cus">
                <div className="title-cus-text">
                    <h1>Our Customer</h1>
                </div>
            </div>
            <section className="content-cus-con">
                <div className="content-up">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Emblem_of_Thammasat_University.svg/1024px-Emblem_of_Thammasat_University.svg.png" alt="" />
                    <img src="https://www.nia.or.th/frontend/article/blroyvsz/editor/files/nia_logo_color.jpg" alt="" />
                    <img src="https://www.jobbkk.com/upload/employer/02/002/00E002/images/57346.png" alt="" />
                </div>
                <div className="content-down">
                    <img src="https://static.wixstatic.com/media/a9275b_43e2b425ed9441aabdb122ccd3986bf7.png/v1/fill/w_250,h_140,al_c/a9275b_43e2b425ed9441aabdb122ccd3986bf7.png" alt="" />
                    <img src="https://www.got.co.th/upload/company/1/2014Get%20On_Full-Color_Logo_png-01.png" alt="" />
                </div>
            </section>
        </div>
    )
}

export default Content
