import React, { useState } from 'react'
import './Header.css'
import logo from '../images/logo.png'
import { Link, Route } from 'react-router-dom'
import { FiMenu, FiX, FiChevronDown } from "react-icons/fi";

function Header() {

    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    return (
        <div className="header">
            <div className="container">
                <div className="header-con">
                    <div className='logo-container'>
                        <a href="#">
                            <img src = {logo} alt="header-logo" />
                        </a>
                        <a href="#">Digital Store Mesh Co.,Ltd.</a>
                    </div>
                    <ul className={click ? "menu active" : "menu"}>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <Link to="/"><a href="#">Home</a></Link>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <Link to="/Aboutus"><a href="#">About Us</a></Link>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <Link to="/Ourproducts"><a href="#">Our Products</a></Link>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <Link to="/Ourservices"><a href="#">Our Services</a></Link>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <Link to="/Publications"><a href="#">Publications</a></Link>
                        </li>
                        <li className="menu-link" onClick={closeMobileMenu}>
                            <a href="/contents">Contents<FiChevronDown /></a>
                        </li>
                    </ul>
                    <div className="mobile-menu" onClick={handleClick}>
                        {click ? (
                            <FiX />
                        ) : (
                            <FiMenu />
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header
