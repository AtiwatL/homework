import React from 'react'
import Header from './Header'
import Footer from './Footer'
import './AboutUspage.css'

let bannerData = {
    title: "About Us"
}

function AboutUspage() {
    return (
        <div>
            <Header />
            <div className="aboutus-bg">
                <div className="aboutus-banner-con">
                    <div className="aboutus-banner-text">
                        <h1>{bannerData.title}</h1>
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="title-con">
                    <div className="title-text">
                        <h1>" สร้างเครือข่ายธุรกิจด้วยการ<br/>
                            เชื่อมโยงข้อมูลที่เหนือกว่า "
                        </h1>
                    </div>
                </div>
                <section className="content-con">
                    <div className="image-l">
                        <img src="https://lh3.googleusercontent.com/nQdC6qcPZlyRvMG0H4VRnDV7CLaeV2lYX6Gc8_0HSqcOB3YrT1Bh8dbJ1g27hwhYexR9HMV_TH2uajZ3zALX88cjkVeIfY9HS6VkpYodNMGIQ3R_H2Fxajz17Lkx70E00w=w1280" alt="" />
                    </div>
                    <div className="content2-r">
                        <h3>ความเชี่ยวชาญของเรา</h3>
                        <p>
                            ทีมงานของเรามีประสบการณ์ในการ<br/>
                            ดำเนินโครงการภาครัฐและเอกชน <br/>
                            ด้านการพัฒนาระบบจัดการข้อมูล<br/>
                            ขนาดใหญ่
                        </p>
                    </div>
                </section>
                <section className="content-con">
                    <div className="content2-l">
                        <img src="https://lh6.googleusercontent.com/uxUo15aS0mF1_0ueYxJhscHbW0EjNLzuV96-gkDPbJxlLRmAg6a51lmV0wkI34EWTU0mvWin9EIuz2TFBdXewOroZ_pdJGnJRgyVnEdaNpvpD3M4JQgtIl5IyRHcGj_CXA=w1280" alt="" />
                    </div>
                    <div className="content2-r">
                        <h3>ความเชี่ยวชาญของเรา</h3>
                        <p>
                            บริษัท ดิจิทัลสโตร์เมช จำกัด ประกอบกิจการให้บริการ<br/>
                            ทำการวิจัยระบบคอมพิวเตอร์ พัฒนาระบบโปรแกรม<br/>
                            คอมพิวเตอร์ โดยใช้เทคโนโลยีล่าสุด วิจัยร่วมกับมหาวิทยาลัยด้าน <br/>
                            Big Data และ Machine Learning เพื่อบริหารจัดการคลังสินค้า<br/>
                            และระบบโลจิสติกส์ โดยให้บริการระบบซอฟต์แวร์จัดการคลัง<br/>
                            สินค้าและระบบโลจิสติกส์เพื่อเชื่อมโยงธุรกิจอย่างมีประสิทธิภาพ <br/>
                        </p>
                    </div>
                </section>
            </div>
            <Footer />
        </div>
    )
}

export default AboutUspage
