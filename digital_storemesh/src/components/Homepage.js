import React from 'react'
import Header from './Header'
import Banner from './Banner'
import Content from './Content'
import Footer from './Footer'
import { Link, Route } from 'react-router-dom'

function Homepage() {
    return (
        <div>
            <Header />
            <Banner />
            <Content />
            <Footer />
        </div>
    )
}

export default Homepage
