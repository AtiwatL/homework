import Homepage from './components/Homepage'
import AboutUspage from './components/AboutUspage';
import { Switch, Link, Route } from 'react-router-dom'
import Publications from './components/Publications';
import Ourproducts from './components/Ourproducts';
import OurService from './components/OurService';
import Content1 from './components/Content1';


function App() {
  return (
    <div>
      <Switch>
        <Route path="/" exact>
          <Homepage />
        </Route>
        <Route path="/Aboutus">
          <AboutUspage />
        </Route>
        <Route path="/Ourproducts">
          <Ourproducts/>
        </Route>
        <Route path="/Ourservices">
          <OurService/>
        </Route>
        <Route path="/Publications">
          <Publications />
        </Route>
        <Route path="/contents">
          <Content1/>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
